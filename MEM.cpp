/*
* MEM.cpp
*
* Created: 11/16/2017 10:59:11 AM
*  Author: Ing.Elvys Collado
*/
#include "MEM.h"
extern String readable2BYTEHEX(unsigned char byte);
extern unsigned char masterTaskMode;
#define PLC 1
#define GSM_PARAMETERS_INDEX 512
#define TIME_FOR_TASK_INDEX 768
#define MAINTENANCE_TASK_INDEX 20000

//-------------- GSM PARAMS -------------//
// GSM start index 500
//CLIENT
extern String SERVEROUT;	 //40
extern String GPRS_APN;		 //40
extern String GPRS_LOGIN;	 //10
extern String GPRS_PASSWORD; //10
extern String GPRS_PORTOUT;	 //02

//SERVER
extern String SERVER_IP;   //40
extern String PATHOUT;	   //40
extern String PHONE1;	   //10
extern String PHONE2;	   //10
extern String PHONE3;	   //10
extern String SERVER_PORT; //02
extern String GPRS_PORTIN; //02

//224

//--------- Ethernet Parameters ----------------//
IPAddress ip(192, 168, 4, 5);		// IP address
IPAddress dnServer(8, 8, 8, 8);		// DNS
IPAddress gateway(192, 168, 4, 1);	// Gateway
IPAddress subnet(255, 255, 255, 0); // Subnet
unsigned short port_in = 80;		// Inbound port
unsigned short port_out = 8081;		// Outbound port
String outbound_svr = "eesupplier.dyndns.org";
char unsigned mac[] = {0xDE, 0xAD, 0xB0, 0xEF, 0xFE, 0x01}; // Default MAC Address

String cs_str = "eesembrujo.dyndns.org:82";
unsigned short cs_port = 80;
unsigned long cs_interval = 1 * 60 * 1000;
unsigned long net_last_time = 0;

unsigned long timeout_ethernet = 30 * 1000;
extern unsigned short port_out;

extern String master_name;

#if PLC == 1
//-------------- PLC PARAMS -------------//
extern unsigned long plc_high_frequency;
extern unsigned long plc_low_frequency;
extern unsigned char plc_trams_param_arm;
extern unsigned char plc_trams_param;
extern unsigned char plc_SS_key[16];
extern unsigned char plc_tx_gain;
extern unsigned char plc_tx_gain_f;
extern unsigned long plc_timeout_uc;
extern unsigned long plc_timeout_net;
extern unsigned long plc_modulation;
extern char master_mode;
#endif

//-----------Times and Minutes task -------//

unsigned char minute_for_energy = 1;
unsigned char minute_for_demand = 2;
unsigned char minute_for_events = 3;
unsigned char minutes_for_allTask = 1;

//---------- Connection String-------------//
extern String cs_str;

extern unsigned long cs_interval;
extern String outbound_svr;
unsigned char net_reconfig = 0;
unsigned long net_reconfig_time = 50;

unsigned char gsm_parameters[250]; /////////////////////////////////////////// Array where GSM parameters
unsigned char network[120];		   ////////////////////////////////////////////////// Array where network parameters
unsigned char plc[50];			   /////////////////////////////////////////////////////// Array where plc parameters
unsigned char task[50];			   ////////////////////////////////////////////////////// Array where tasks parameters
unsigned char TimesForTask[50];	   ////////////////////////////////////////////////////// Array where tasks times and minutes

struct MEMORY NORM = MEMconfig(3, EEPROM_MEM_CS_PIN);
struct MEMORY UID = MEMconfig(1, UID_MEM_CS_PIN);

unsigned long meter_index = 0;
unsigned char meters_qty = 0;
unsigned char master_id = 0x0D; // default for test electricales //
unsigned char master_maintenance_tasks[60];

unsigned int master_change_father_time = 300;
unsigned int meter_change_father_time = 300;
unsigned int incomunication_task_time = 300;

enum size_of_data
{
	name_long = 40 + 1,
	name_short = 10 + 1,
	pass_short = name_short,
	number = 2

};

struct MEMORY MEMconfig(unsigned char size, unsigned char slave_pin)
{
	struct MEMORY mem;
	mem.size = size;
	mem.slave_pin = slave_pin;
	return mem;
}
//SPIClass
SPISettings SPISettings_UID(20000000, MSBFIRST, SPI_MODE0);
SPISettings SPISettings_NORM(20000000, MSBFIRST, SPI_MODE0);

unsigned char readMEM(
	unsigned long address,
	MEMORY mem,
	SPISettings _SPISettings)
{

	unsigned char data;
	SPI.beginTransaction(_SPISettings);
	digitalWrite(mem.slave_pin, LOW);
	SPI.transfer(WREN);
	digitalWrite(mem.slave_pin, HIGH);
	delay(10);
	digitalWrite(mem.slave_pin, LOW);
	SPI.transfer(READ);
	if (mem.size >= 3)
	{
		SPI.transfer((char)(address >> 16));
	}
	if (mem.size >= 2)
	{
		SPI.transfer((char)(address >> 8));
	}
	SPI.transfer((char)(address));
	data = SPI.transfer(0xFF);
	digitalWrite(mem.slave_pin, HIGH);
	SPI.endTransaction();
	return data;
}

void writeMEM(
	unsigned int address,
	MEMORY mem,
	SPISettings _SPISettings,
	unsigned char data)
{

	SPI.beginTransaction(_SPISettings);
	digitalWrite(mem.slave_pin, LOW);
	SPI.transfer(WREN);
	digitalWrite(mem.slave_pin, HIGH);
	delay(10);
	digitalWrite(mem.slave_pin, LOW);
	SPI.transfer(WRITE);
	if (mem.size >= 3)
	{
		SPI.transfer((char)(address >> 16));
	}
	if (mem.size >= 2)
	{
		SPI.transfer((char)(address >> 8));
	}
	SPI.transfer((char)(address));
	SPI.transfer(data);
	digitalWrite(mem.slave_pin, HIGH);
	SPI.endTransaction();
	delay(10);
}

void addToMEM(
	unsigned int id,
	unsigned long s_number,
	unsigned long m_number,
	unsigned long p_number,
	unsigned long p_number2,
	unsigned char isFather)
{
	//unsigned int pos = memory_index +  (id * 0x0F) - 0x0C;

	meter_index = MEMORY_INDEX + readMEM(MEMORY_INDEX, NORM, SPISettings_NORM) * RECORD_SIZE + 0x01;

	// Add Index of the meter in Memory
	Serial.print("meter_index");
	Serial.println(meter_index, DEC);

	meter_index = meter_index + 1;

	writeMEM(meter_index++, NORM, SPISettings_NORM, id);

	// Add activity status "TRUE: 1"
	writeMEM(meter_index++, NORM, SPISettings_NORM, 0x01);

	// Add to Memory Board Serial Number

	char four = (s_number & 0xFF);
	char three = ((s_number >> 8) & 0xFF);
	char two = ((s_number >> 16) & 0xFF);
	char one = ((s_number >> 24) & 0xFF);

	writeMEM(meter_index++, NORM, SPISettings_NORM, one);
	writeMEM(meter_index++, NORM, SPISettings_NORM, two);
	writeMEM(meter_index++, NORM, SPISettings_NORM, three);
	writeMEM(meter_index++, NORM, SPISettings_NORM, four);

	// Add to Memory Meter Serial Number

	four = (m_number & 0xFF);
	three = ((m_number >> 8) & 0xFF);
	two = ((m_number >> 16) & 0xFF);
	one = ((m_number >> 24) & 0xFF);

	writeMEM(meter_index++, NORM, SPISettings_NORM, one);
	writeMEM(meter_index++, NORM, SPISettings_NORM, two);
	writeMEM(meter_index++, NORM, SPISettings_NORM, three);
	writeMEM(meter_index++, NORM, SPISettings_NORM, four);

	// Add to Memory Father Serial Number

	four = (p_number & 0xFF);
	three = ((p_number >> 8) & 0xFF);
	two = ((p_number >> 16) & 0xFF);
	one = ((p_number >> 24) & 0xFF);

	writeMEM(meter_index++, NORM, SPISettings_NORM, one);
	writeMEM(meter_index++, NORM, SPISettings_NORM, two);
	writeMEM(meter_index++, NORM, SPISettings_NORM, three);
	writeMEM(meter_index++, NORM, SPISettings_NORM, four);

	four = (p_number2 & 0xFF);
	three = ((p_number2 >> 8) & 0xFF);
	two = ((p_number2 >> 16) & 0xFF);
	one = ((p_number2 >> 24) & 0xFF);

	writeMEM(meter_index++, NORM, SPISettings_NORM, one);
	writeMEM(meter_index++, NORM, SPISettings_NORM, two);
	writeMEM(meter_index++, NORM, SPISettings_NORM, three);
	writeMEM(meter_index++, NORM, SPISettings_NORM, four);
	//****** Add to Memory if the meter is a father (Not a father by default)* old**
	// adding father state

	writeMEM(meter_index++, NORM, SPISettings_NORM, isFather);

	//updating and storing meter quantity
	meters_qty += 1;
	writeMEM(MEMORY_INDEX, NORM, SPISettings_NORM, meters_qty);
}

void updateMeterInMEM(
	unsigned int id,
	unsigned long s_number,
	unsigned long m_number,
	unsigned long p_number,
	unsigned long p_number2)
{
	unsigned int _meter_index = MEMORY_INDEX + (id * RECORD_SIZE) - 16; //16 to move to active byte

	//_meter_index = memory_index + readMEM(memory_index, NORM, SPISettings_NORM) * 0x0F + 0x01;

	// Add Index of the meter in Memory
	Serial.print("_meter_index");
	Serial.println(_meter_index, DEC);
	_meter_index = _meter_index + 1;
	writeMEM(_meter_index++, NORM, SPISettings_NORM, id);

	// Add activity status "TRUE: 1"
	writeMEM(_meter_index++, NORM, SPISettings_NORM, 0x01);

	// Add to Memory Board Serial Number

	char four = (s_number & 0xFF);
	char three = ((s_number >> 8) & 0xFF);
	char two = ((s_number >> 16) & 0xFF);
	char one = ((s_number >> 24) & 0xFF);

	writeMEM(_meter_index++, NORM, SPISettings_NORM, one);
	writeMEM(_meter_index++, NORM, SPISettings_NORM, two);
	writeMEM(_meter_index++, NORM, SPISettings_NORM, three);
	writeMEM(_meter_index++, NORM, SPISettings_NORM, four);

	// Add to Memory Meter Serial Number

	four = (m_number & 0xFF);
	three = ((m_number >> 8) & 0xFF);
	two = ((m_number >> 16) & 0xFF);
	one = ((m_number >> 24) & 0xFF);

	writeMEM(_meter_index++, NORM, SPISettings_NORM, one);
	writeMEM(_meter_index++, NORM, SPISettings_NORM, two);
	writeMEM(_meter_index++, NORM, SPISettings_NORM, three);
	writeMEM(_meter_index++, NORM, SPISettings_NORM, four);

	// Add to Memory Father Serial Number

	four = (p_number & 0xFF);
	three = ((p_number >> 8) & 0xFF);
	two = ((p_number >> 16) & 0xFF);
	one = ((p_number >> 24) & 0xFF);

	writeMEM(_meter_index++, NORM, SPISettings_NORM, one);
	writeMEM(_meter_index++, NORM, SPISettings_NORM, two);
	writeMEM(_meter_index++, NORM, SPISettings_NORM, three);
	writeMEM(_meter_index++, NORM, SPISettings_NORM, four);

	four = (p_number2 & 0xFF);
	three = ((p_number2 >> 8) & 0xFF);
	two = ((p_number2 >> 16) & 0xFF);
	one = ((p_number2 >> 24) & 0xFF);

	writeMEM(_meter_index++, NORM, SPISettings_NORM, one);
	writeMEM(_meter_index++, NORM, SPISettings_NORM, two);
	writeMEM(_meter_index++, NORM, SPISettings_NORM, three);
	writeMEM(_meter_index++, NORM, SPISettings_NORM, four);

	// Add to Memory if the meter is a father (Not a father by default)
	//writeMEM(_meter_index + 15, NORM, SPISettings_NORM, 0x00);

	//updating and storing meter quantity
	//meters_qty += 1;
	//writeMEM(memory_index, NORM, SPISettings_NORM, meters_qty);
}

void disposeMEM(void)
{
	//Reset the index
	writeMEM(MEMORY_INDEX, NORM, SPISettings_NORM, 0x00);
	meters_qty = 0;
	meter_index = 0;
}

unsigned char getMEMIndex()
{
	return readMEM(MEMORY_INDEX, NORM, SPISettings_NORM);
}

void changeMetersState(unsigned int id, unsigned char state)
{
	//To update the status of the meter
	unsigned int pos = MEMORY_INDEX + (id * RECORD_SIZE) - 16;
	unsigned char current_state = readMEM(pos, NORM, SPISettings_NORM);
	current_state = (state & 0x01) ? current_state | 1 : current_state & ~1;
	writeMEM(pos, NORM, SPISettings_NORM, state);
	Serial.print("meter index: " + String(pos, DEC) + "state: " + String(readMEM(pos, NORM, SPISettings_NORM)));
}
void updateFatherMEM(unsigned int id, unsigned char selector)
{

	unsigned int pos = MEMORY_INDEX + (id * RECORD_SIZE) - 16;
	unsigned char current_state = readMEM(pos, NORM, SPISettings_NORM);
	current_state = (current_state & 0x0F) | ((selector << 4) & 0xF0);
	writeMEM(pos, NORM, SPISettings_NORM, current_state);
}

void printSerialListFromMEM()
{

	unsigned char qty = readMEM(MEMORY_INDEX, NORM, SPISettings_NORM);
	//Serial.println("qty: " + String(qty, DEC));
	unsigned int i = 0;
	debugger_port.println("id-ST- Board - Meter - Father - IsFather");
	unsigned short num = 0;

	for (unsigned int m = MEMORY_INDEX + 2; m <= qty * (RECORD_SIZE) + 1 + MEMORY_INDEX; m++)
	{
		i++;
		if (i == 1)
		{
			num++;
			debugger_port.print(num);
			debugger_port.print("-");
			debugger_port.print(readMEM(m, NORM, SPISettings_NORM), HEX);
			debugger_port.print("-");
		}
		else if (i == 2)
		{
			debugger_port.print(readMEM(m, NORM, SPISettings_NORM), HEX);
			debugger_port.print("-");
		}
		else if (i < 6)
		{
			debugger_port.print(readMEM(m, NORM, SPISettings_NORM), HEX);
		}
		else if (i == 6)
		{
			debugger_port.print(readMEM(m, NORM, SPISettings_NORM), HEX);
			debugger_port.print("-");
		}
		else if (i < 10)
		{
			debugger_port.print(readMEM(m, NORM, SPISettings_NORM), HEX);
		}
		else if (i == 10)
		{
			debugger_port.print(readMEM(m, NORM, SPISettings_NORM), HEX);
			debugger_port.print("-");
		}
		else if (i < 14)
		{
			debugger_port.print(readMEM(m, NORM, SPISettings_NORM), HEX);
		}
		else if (i == 14)
		{
			debugger_port.print(readMEM(m, NORM, SPISettings_NORM), HEX);
			debugger_port.print("-");
		}
		else if (i == 15)
		{
			debugger_port.print(readMEM(m, NORM, SPISettings_NORM), HEX);
			debugger_port.print("-");
			//i = 0;
		}
		else if (i < 19)
		{
			debugger_port.print(readMEM(m, NORM, SPISettings_NORM), HEX);
		}
		else if (i == 19)
		{
			debugger_port.println(readMEM(m, NORM, SPISettings_NORM), HEX);
			//	debugger_port.print("-");
			i = 0;
		}
		//
		//else if (i < 23)
		//{
		//debugger_port.print(readMEM(m, NORM, SPISettings_NORM), HEX);
		//
		//}
		//else if (i == 23)
		//{
		//debugger_port.println(readMEM(m, NORM, SPISettings_NORM), HEX);
		//i=0;
		//}
		//
	}
}

linked_list copyMEMToRAM(
	linked_list _list)
{
	char record[RECORD_SIZE];

	unsigned char qty = readMEM(MEMORY_INDEX, NORM, SPISettings_NORM);
	Serial.println("Listado de medidores asociados: " + String(qty));
	unsigned int i = 0;
	unsigned int imprimir = 0;

	Serial.print("id-");
	Serial.print(" Meter-");
	Serial.print(" IsFather-");
	Serial.println(" State");

	unsigned long limited = (qty * RECORD_SIZE) + 1 + MEMORY_INDEX;

	for (unsigned int m = MEMORY_INDEX + 2; m <= limited; m++)
	{ //All records containg meters
		//1000+2 ; 1002 <= 13*23+4 +1000

		//Serial.println("m: " + String(m,DEC) + "+ limited: " + String(limited));
		i++;
		record[i - 1] = readMEM(m, NORM, SPISettings_NORM);

		if (i == 2 && (record[1] & 0x01) == 1)
		{
			imprimir = 1;
			Serial.print("Medidor Activo:");
		}
		else if (i > 1 && !imprimir)
		{
			i = 0;
			m += (RECORD_SIZE - 2);
			record[1] = 0xff;
			Serial.print(int(record[0]));
			Serial.print("-");
			Serial.print(int(record[1]));
			Serial.print("skip");
			Serial.println(m);
		}
		else if (i == RECORD_SIZE && imprimir == 1)
		{
			i = 0;

			unsigned long tarjeta_hijo = ((record[2] << 24) & 0xFFFFFFFF) + ((record[3] << 16) & 0xFFFFFF) + ((record[4] << 8) & 0xFFFF) + ((record[5]) & 0xFF);

			unsigned long serial_hijo = ((record[6] << 24) & 0xFFFFFFFF) + ((record[7] << 16) & 0xFFFFFF) + ((record[8] << 8) & 0xFFFF) + ((record[9]) & 0xFF);

			unsigned long medidor_padre = ((record[10] << 24) & 0xFFFFFFFF) + ((record[11] << 16) & 0xFFFFFF) + ((record[12] << 8) & 0xFFFF) + ((record[13]) & 0xFF);

			unsigned long medidor_padre2 = ((record[14] << 24) & 0xFFFFFFFF) + ((record[15] << 16) & 0xFFFFFF) + ((record[16] << 8) & 0xFFFF) + ((record[17]) & 0xFF);

			_list.p_node = _list.addSerial(tarjeta_hijo, serial_hijo, medidor_padre, medidor_padre2, record[1], record[18], record[0], 0);
			//if (record[14] == 1) _list.p_node_fathers = _list.addFathersToFatherList(serial_hijo);

			Serial.print(m);
			Serial.print("-");
			Serial.print(int(record[0]));
			Serial.print("-");
			Serial.print(record[1], HEX);
			Serial.print("-");
			Serial.print(tarjeta_hijo);
			Serial.print("-");
			Serial.print(serial_hijo);
			Serial.print("-");
			Serial.print(medidor_padre);
			Serial.print("-");
			Serial.print(medidor_padre2);
			Serial.print("-");
			Serial.println(record[18], HEX);

			imprimir = 0;
		}
	}
	return _list;
}

unsigned char updateFatherEEPROM(
	unsigned long s_number,
	unsigned long p_number,
	unsigned char father_selector)
{

	char record[RECORD_SIZE];
	unsigned char qty = readMEM(MEMORY_INDEX, NORM, SPISettings_NORM);
	unsigned long i = 0;
	unsigned long imprimir = 0;
	unsigned long s_number_aux = 0;
	unsigned long p_number_aux = 0;
	//Serial.println("qty: " + String(qty));

	setFatherEEPROM(p_number);
	for (unsigned long m = MEMORY_INDEX + 2; m <= qty * RECORD_SIZE + 1 + MEMORY_INDEX; m++)
	{
		i++;

		record[i - 1] = readMEM(m, NORM, SPISettings_NORM);

		//Serial.println("record[1]: " + String(record[1],DEC));

		if (i == 7)
		{
			s_number_aux = ((record[5] << 0) & 0xFF) + ((record[4] << 8) & 0xFFFF) + ((record[3] << 16) & 0xFFFFFF) + ((record[2] << 24) & 0xFFFFFFFF);
			if (int(record[1] & 0x01) == 1)
			{
				if (s_number_aux == s_number)
				{
					imprimir = 1;
					Serial.print("A la tarjeta: ");
					Serial.print(s_number_aux);
				}
			}
			else
			{
				i = 0;
				m += (RECORD_SIZE - 7);
				s_number_aux = 0;
				p_number_aux = 0;
			}
		}

		else if (i > 6 && !imprimir)
		{
			i = 0;
			m += (RECORD_SIZE - 8);
			s_number_aux = 0;
			p_number_aux = 0;
		}
		else if (i == RECORD_SIZE && imprimir == 1)
		{
			i = 0;
			p_number_aux = p_number;
			Serial.print(" se le ha asignado el padre: ");
			Serial.print(p_number_aux);

			p_number_aux = ((record[13] << 0) & 0xFF) + ((record[12] << 8) & 0xFFFF) + ((record[11] << 16) & 0xFFFFFF) + ((record[10] << 24) & 0xFFFFFFFF);

			//Serial.println(" p_number_aux: " + String(p_number_aux));

			if (!father_selector)
			{
				m -= 4;
			}

			writeMEM(m - 4, NORM, SPISettings_NORM, (p_number >> 24) & 0xff);
			writeMEM(m - 3, NORM, SPISettings_NORM, (p_number >> 16) & 0xff);
			writeMEM(m - 2, NORM, SPISettings_NORM, (p_number >> 8) & 0xff);
			writeMEM(m - 1, NORM, SPISettings_NORM, p_number & 0xff);

			imprimir = 0;
			return 1;
		}
		else
		{
			;
		}
	}

	return 0;
}

unsigned char setFatherEEPROM(
	unsigned long p_number)
{

	char record[RECORD_SIZE];
	unsigned char qty = readMEM(MEMORY_INDEX, NORM, SPISettings_NORM);
	unsigned long i = 0;
	unsigned long imprimir = 0;
	unsigned long s_number_aux = 0;

	for (unsigned long m = MEMORY_INDEX + 2; m <= (qty * RECORD_SIZE) + 1 + MEMORY_INDEX; m++)
	{
		i++;
		record[i - 1] = readMEM(m, NORM, SPISettings_NORM);

		if (i == 7)
		{
			s_number_aux = ((record[5] << 0) & 0xFF) + ((record[4] << 8) & 0xFFFF) + ((record[3] << 16) & 0xFFFFFF) + ((record[2] << 24) & 0xFFFFFFFF);

			if (int(record[1] & 0x01) == 1)
			{
				if (s_number_aux == p_number)
				{
					imprimir = 1;
					Serial.print("la tarjeta:");
					Serial.print(s_number_aux);
				}
			}
			else
			{
				i = 0;
				m += (RECORD_SIZE - 7);
				s_number_aux = 0;
			}
		}
		else if (i > 6 && !imprimir)
		{
			i = 0;
			m += (RECORD_SIZE - 8);
			s_number_aux = 0;
		}
		else if (i == RECORD_SIZE && imprimir == 1)
		{
			i = 0;

			Serial.println(" ahora es padre padre!");
			writeMEM(m, NORM, SPISettings_NORM, 1);
			imprimir = 0;
			return 1;
		}
		else
		{
			;
		}
	}

	return 0;
}

void loadNetworkParam(void)
{
#if DEVICE == 0
	//IP
	ip[0] = network[3];
	ip[1] = network[2];
	ip[2] = network[1];
	ip[3] = network[0];

	//dns
	dnServer[0] = network[7];
	dnServer[1] = network[6];
	dnServer[2] = network[5];
	dnServer[3] = network[4];

	// gateway address:
	gateway[0] = network[11];
	gateway[1] = network[10];
	gateway[2] = network[9];
	gateway[3] = network[8];

	// the subnet:
	subnet[0] = network[15];
	subnet[1] = network[14];
	subnet[2] = network[13];
	subnet[3] = network[12];

#else if DEVICE == 0

	//
	//mac[0] =  readMEM(0xFA, UID, SPISettings_UID);
	//mac[1] =  readMEM(0xFB, UID, SPISettings_UID);
	//mac[2] =  readMEM(0xFC, UID, SPISettings_UID);
	//mac[3] =  readMEM(0xFD, UID, SPISettings_UID);
	//mac[4] =  readMEM(0xFE, UID, SPISettings_UID);
	//mac[5] =  readMEM(0xFF, UID, SPISettings_UID);

	mac[0] = network[16];
	mac[1] = network[17];
	mac[2] = network[18];
	mac[3] = network[19];
	mac[4] = network[20];
	mac[5] = network[21];

	//master_id =  mac[5]; // 0x0D; //mac[5];
	master_name = readable2BYTEHEX(network[16]) + ":" + readable2BYTEHEX(network[17]) + ":" + readable2BYTEHEX(network[18]) + ":" + readable2BYTEHEX(network[19]) + ":" + readable2BYTEHEX(network[20]) + ":" + readable2BYTEHEX(network[21]);

	//Inbound port
	port_in = network[22] << 8 | network[23];

	//Ethernet Timeout
	timeout_ethernet = (network[24] << 8 | network[25]) * 1000;

	//Server outbound
	outbound_svr = "";
	for (int r = 0; r < 40; r++)
	{
		char character = network[26 + r];
		if (character > 31 && character < 127)
		{
			outbound_svr += character;
		}
	}
	//Outbound port
	port_out = network[66] << 8 | network[67];

	//Connection String
	cs_str = "";
	for (int r = 0; r < 40; r++)
	{
		char character = network[68 + r];
		if (character > 31 && character < 127)
		{
			cs_str += character;
		}
	}

	if (network[108] == 0)
	{
		network[108] = 1;
	}
	cs_interval = network[108] * 60 * 1000;

	net_reconfig_time = ((network[112] << 0) & 0xFF) + ((network[111] << 8) & 0xFFFF) + ((network[110] << 16) & 0xFFFFFF) + ((network[109] << 24) & 0xFFFFFFFF);

	if (net_reconfig_time)
	{
		net_reconfig = 1;
	}
	else
	{
		net_reconfig = 0;
	}

#endif
}

void saveNetworkParam(void)
{
	for (int r = 0; r < 120; r++)
	{
		writeMEM(256 + r, NORM, SPISettings_NORM, network[r]);
		delay(10);
	}
}

void setInitialConditions(void) //Loading default parameters to EEPROM
{
	//initial_configuration = true;
	//inicio = -1; //Master Mute

	/////////////////////////////////////////////////// Network Default Parameters

	//IP
	network[0] = ip[3];
	network[1] = ip[2];
	network[2] = ip[1];
	network[3] = ip[0];

	//dns
	network[4] = dnServer[3];
	network[5] = dnServer[2];
	network[6] = dnServer[1];
	network[7] = dnServer[0];

	// gateway address:
	network[8] = gateway[3];
	network[9] = gateway[2];
	network[10] = gateway[1];
	network[11] = gateway[0];

	// the subnet:
	network[12] = subnet[3];
	network[13] = subnet[2];
	network[14] = subnet[1];
	network[15] = subnet[0];

	//MAC

	network[16] = readMEM(0xFA, UID, SPISettings_UID);
	network[17] = readMEM(0xFB, UID, SPISettings_UID);
	network[18] = readMEM(0xFC, UID, SPISettings_UID);
	network[19] = readMEM(0xFD, UID, SPISettings_UID);
	network[20] = readMEM(0xFE, UID, SPISettings_UID);
	network[21] = readMEM(0xFF, UID, SPISettings_UID);

	master_name = readable2BYTEHEX(network[16]) + ":" + readable2BYTEHEX(network[17]) + ":" + readable2BYTEHEX(network[18]) + ":" + readable2BYTEHEX(network[19]) + ":" + readable2BYTEHEX(network[20]) + ":" + readable2BYTEHEX(network[21]);

	//Inbound port
	network[22] = port_in >> 8;
	network[23] = port_in;

	//Timeout Ethernet
	network[24] = (timeout_ethernet / 1000) >> 8;
	network[25] = timeout_ethernet / 1000;

	// Outbound Server
	unsigned char len = outbound_svr.length();
	for (int r = 0; r < 40; r++)
	{
		if (r < len)
		{
			network[26 + r] = outbound_svr[r];
		}
		else
		{
			network[26 + r] = 0;
		}
	}

	//Outbound port
	network[66] = (port_out >> 8) & 0xFF;
	network[67] = (port_out)&0xFF;

	//Connection String
	len = cs_str.length();
	for (int r = 0; r < 40; r++)
	{
		if (r < len)
		{
			network[68 + r] = cs_str[r];
		}
		else
		{
			network[68 + r] = 0;
		}
	}
	// Connection String Interval
	if (cs_interval / (60 * 1000) < 1)
	{
		cs_interval = 60000;
	}
	network[108] = cs_interval / (60 * 1000);

	network[109] = (net_reconfig_time >> 24) & 0xff;
	network[110] = (net_reconfig_time >> 16) & 0xff;
	network[111] = (net_reconfig_time >> 8) & 0xff;
	network[112] = (net_reconfig_time)&0xff;

	loadNetworkParam();
	saveNetworkParam();

#if PLC == 1
	/////////////////////////////////////////////////// PLC Default Parameters
	plc[0] = (plc_high_frequency / 1000) & 0xFF;
	plc[1] = (plc_low_frequency / 1000) & 0xFF;
	plc[2] = plc_tx_gain;
	plc[3] = plc_tx_gain_f;

	plc[4] = (plc_timeout_uc / 1000) >> 8;
	plc[5] = plc_timeout_uc / 1000;

	plc[6] = (plc_timeout_net / 1000) >> 8;
	plc[7] = plc_timeout_net / 1000;

	plc[8] = plc_SS_key[0];
	plc[9] = plc_SS_key[1];
	plc[10] = plc_SS_key[2];
	plc[11] = plc_SS_key[3];
	plc[12] = plc_SS_key[4];
	plc[13] = plc_SS_key[5];
	plc[14] = plc_SS_key[6];
	plc[15] = plc_SS_key[7];
	plc[16] = plc_SS_key[8];
	plc[17] = plc_SS_key[9];
	plc[18] = plc_SS_key[10];
	plc[19] = plc_SS_key[11];
	plc[20] = plc_SS_key[12];
	plc[21] = plc_SS_key[13];
	plc[22] = plc_SS_key[14];
	plc[23] = plc_SS_key[15];
	plc[24] = master_id;
	plc[25] = plc_modulation;

	loadPLCParam();

	savePLCParam();

	/////////////////////////////////////////////////// Task Default Parameters

	task[0] = 0;
	task[1] = 0;
	task[2] = 100;
	task[3] = 3;
	task[4] = 0;
	task[5] = 0;
	task[6] = 100;
	task[7] = 3;
	task[8] = 0;
	task[9] = 0;
	task[10] = 5;
	task[11] = 3;
	task[12] = 0;
	task[13] = 0;
	task[14] = 6;
	task[15] = 0;

	//load_tasks_param();
	//save_tasks_param_to_mem();
	loadGSMParamFromVars(); //set default params
	saveGSMParam();			//save default params
	readGSMParamFromMem();
	loadGSMParamToVar();

	loadTaskParamFromVars(); //set default params
	saveTaskParam();		 //save default params
	readTaskParamFromMem();
	loadTaskParamFromVars();
	clearAllMaintenanceTask();
	writeMEM(MEMORY_INDEX, NORM, SPISettings_NORM, 0);
#endif
}

#if PLC == 1
void loadPLCParam(void)
{

	plc_modulation = plc[25];
	plc_high_frequency = plc[0] * 1000;
	plc_low_frequency = plc[1] * 1000;

	if (plc[0] > 90)
	{
		plc_trams_param_arm = plc_modulation | 0x08;
		plc_trams_param = plc_modulation | 0x0C;
	}
	else
	{
		plc_trams_param_arm = plc_modulation | 0x0C;
		plc_trams_param = plc_modulation | 0x08;
	}

	plc_tx_gain = plc[2];
	plc_tx_gain_f = plc[3];
	plc_timeout_uc = (plc[4] << 8 | plc[5]) * 1000;
	plc_timeout_net = (plc[6] << 8 | plc[7]) * 1000;

	for (int r = 0; r < 16; r++) // Loading SS_key
	{
		plc_SS_key[r] = plc[r + 8];
	}
	master_id = plc[24];
}

void savePLCParam(void)
{
	for (int r = 0; r < 10 + 16; r++)
	{
		writeMEM(376 + r, NORM, SPISettings_NORM, plc[r]);
		Serial.println(String(r) + "-" + String(plc[r]));
		delay(10);
	}
}
#endif

String getNetInJSON(void)
{
	String auxiliar_data = "";
	DynamicJsonBuffer jsonBuffer;
	JsonObject &root = jsonBuffer.createObject();
	JsonObject &form = jsonBuffer.createObject();
	root["f"] = form;

	JsonArray &ip_array = form.createNestedArray("Ip Address");
	for (unsigned char i = 0; i <= 3; i++)
	{
		JsonObject &ip_obj = jsonBuffer.createObject();
		ip_obj["t"] = 1;
		ip_obj["n"] = "ip" + String(i + 1, DEC);
		ip_obj["v"] = ip[i];
		ip_array.add(ip_obj);
	}

	JsonArray &dns_array = form.createNestedArray("DNS");
	for (unsigned char i = 0; i <= 3; i++)
	{
		JsonObject &dns_obj = jsonBuffer.createObject();
		dns_obj["t"] = 1;
		dns_obj["n"] = "dns" + String(i + 1, DEC);
		dns_obj["v"] = dnServer[i];
		dns_array.add(dns_obj);
	}
	JsonArray &gtw_array = form.createNestedArray("Gateway");
	for (unsigned char i = 0; i <= 3; i++)
	{
		JsonObject &gtw_obj = jsonBuffer.createObject();
		gtw_obj["t"] = 1;
		gtw_obj["n"] = "gw" + String(i + 1, DEC);
		gtw_obj["v"] = gateway[i];
		gtw_array.add(gtw_obj);
	}
	JsonArray &snt_array = form.createNestedArray("Subnet");
	for (unsigned char i = 0; i <= 3; i++)
	{
		JsonObject &snt_obj = jsonBuffer.createObject();
		snt_obj["t"] = 1;
		snt_obj["n"] = "sn" + String(i + 1, DEC);
		snt_obj["v"] = subnet[i];
		snt_array.add(snt_obj);
	}
	JsonArray &mac_array = form.createNestedArray("MAC Address");
	for (unsigned char i = 0; i <= 5; i++)
	{
		JsonObject &mac_obj = jsonBuffer.createObject();
		mac_obj["t"] = 1;
		mac_obj["n"] = "mac" + String(i + 1, DEC);
		mac_obj["v"] = String(mac[i], HEX);
		mac_array.add(mac_obj);
	}
	JsonArray &ptin_array = form.createNestedArray("PORT IN");

	JsonObject &ptin_obj = jsonBuffer.createObject();
	ptin_obj["t"] = 1;
	ptin_obj["n"] = "ptin";
	ptin_obj["v"] = port_in;
	ptin_array.add(ptin_obj);

	JsonObject &ptin_obj2 = jsonBuffer.createObject();
	ptin_obj2["t"] = 1;
	ptin_obj2["n"] = "to_eth";
	ptin_obj2["v"] = timeout_ethernet / 1000;
	ptin_obj2["ti"] = "TO_ETH";
	ptin_array.add(ptin_obj2);

	JsonArray &svr_array = form.createNestedArray("Server");

	JsonObject &svr_obj = jsonBuffer.createObject();
	svr_obj["t"] = 1;
	svr_obj["n"] = "server";
	svr_obj["v"] = outbound_svr;
	svr_obj["s"] = 2;
	svr_array.add(svr_obj);

	JsonObject &svr_obj2 = jsonBuffer.createObject();
	svr_obj2["t"] = 1;
	svr_obj2["n"] = "ptou";
	svr_obj2["v"] = port_out;
	svr_obj2["ti"] = "Port Out";
	svr_array.add(svr_obj2);

	JsonArray &cs_array = form.createNestedArray("Connection String");

	JsonObject &cs_obj = jsonBuffer.createObject();
	cs_obj["t"] = 1;
	cs_obj["n"] = "cstr";
	cs_obj["v"] = cs_str;
	cs_obj["s"] = 2;
	cs_array.add(cs_obj);

	JsonObject &cs_obj3 = jsonBuffer.createObject();
	cs_obj3["t"] = 1;
	cs_obj3["n"] = "ics";
	cs_obj3["v"] = cs_interval / (60 * 1000);
	cs_obj3["ti"] = "CS Interval";
	cs_array.add(cs_obj3);

	JsonArray &nr_array = form.createNestedArray("Net Reconfig Time");

	JsonObject &nrt_obj = jsonBuffer.createObject();
	nrt_obj["t"] = 1;
	nrt_obj["n"] = "nrt";
	nrt_obj["v"] = net_reconfig_time;
	nrt_obj["s"] = 1;
	nr_array.add(nrt_obj);

	root.printTo(auxiliar_data);
	jsonBuffer.clear();

	return auxiliar_data;
}

#if PLC == 1
String getPLCInJSON(void)
{
	String auxiliar_data = "";
	DynamicJsonBuffer jsonBuffer;
	JsonObject &root = jsonBuffer.createObject();
	JsonObject &form = jsonBuffer.createObject();
	root["f"] = form;

	JsonArray &hf_array = form.createNestedArray("HIGH FREQ");

	JsonObject &hf_obj = jsonBuffer.createObject();
	hf_obj["t"] = 1;
	hf_obj["n"] = "HF";
	hf_obj["v"] = plc_high_frequency / 1000;
	hf_array.add(hf_obj);

	JsonObject &hf_obj2 = jsonBuffer.createObject();
	hf_obj2["t"] = 1;
	hf_obj2["n"] = "LF";
	hf_obj2["v"] = plc_low_frequency / 1000;
	hf_obj2["ti"] = "LOW FREQ";
	hf_array.add(hf_obj2);

	JsonArray &txgn_array = form.createNestedArray("TX_GAIN_N");
	JsonObject &txgn_obj = jsonBuffer.createObject();
	txgn_obj["t"] = 1;
	txgn_obj["n"] = "TXN";
	txgn_obj["v"] = String(plc_tx_gain, HEX);
	txgn_array.add(txgn_obj);

	JsonObject &txgn_obj2 = jsonBuffer.createObject();
	txgn_obj2["t"] = 1;
	txgn_obj2["n"] = "TXR";
	txgn_obj2["v"] = String(plc_tx_gain_f, HEX);
	txgn_obj2["ti"] = "TX_GAIN_R";
	txgn_array.add(txgn_obj2);

	JsonArray &to_array = form.createNestedArray("TO_MODEM");
	JsonObject &to_obj = jsonBuffer.createObject();
	to_obj["t"] = 1;
	to_obj["n"] = "TOM";
	to_obj["v"] = plc_timeout_uc / 1000;
	to_array.add(to_obj);

	JsonObject &to_obj2 = jsonBuffer.createObject();
	to_obj2["t"] = 1;
	to_obj2["n"] = "TP";
	to_obj2["v"] = plc_timeout_net / 1000;
	to_obj2["ti"] = "TO_PLC";
	to_array.add(to_obj2);

	JsonArray &mi_array = form.createNestedArray("MASTER_ID");
	JsonObject &mi_obj = jsonBuffer.createObject();
	mi_obj["t"] = 1;
	mi_obj["n"] = "MI";
	mi_obj["v"] = String(master_id, DEC);
	mi_array.add(mi_obj);

	JsonArray &ss_key_array = form.createNestedArray("SS_key");
	JsonObject &ss_key_obj = jsonBuffer.createObject();
	String ss_key_string = "";
	for (unsigned char i = 0; i < 16; i++)
	{
		ss_key_string += readable2BYTEHEX(plc_SS_key[i]);
	}
	ss_key_obj["t"] = 1;
	ss_key_obj["n"] = "sskey";
	ss_key_obj["v"] = ss_key_string;
	ss_key_obj["s"] = 2;
	ss_key_array.add(ss_key_obj);

	root.printTo(auxiliar_data);
	jsonBuffer.clear();

	return auxiliar_data;
}

#endif
void readParamFromMEM()
{

	//	masterTaskMode = (int8_t)readMEM(0, NORM, SPISettings_NORM); ///////////////////////////////////// Reading Master State

	for (int r = 0; r < 120; r++)
	{ ///////////////////////// Reading Network parameters

		network[r] = readMEM(256 + r, NORM, SPISettings_NORM);
		//Serial.println(network[r],HEX);
	}
#if PLC == 1
	for (int r = 0; r < 10 + 16; r++)
	{ ///////////////////////// Reading PLC parameters

		plc[r] = readMEM(376 + r, NORM, SPISettings_NORM);
		//Serial.println(plc[r],HEX);
	}
#endif
	/*for (int r = 0; r < 16; r++){/////////////////////////// Reading Tasks parameters
	
	task[r] = readMEM(426+r , NORM, SPISettings_NORM);
	//Serial.println(task[r],HEX);
	}*/
	loadPLCParam();
}

void readPLCparam()
{

	for (int r = 0; r < 10 + 16; r++)
	{ ///////////////////////// Reading PLC parameters

		plc[r] = readMEM(376 + r, NORM, SPISettings_NORM);
		Serial.println(String(r) + "-" + String(plc[r]));
	}
}

void loadPLCParamsFromPLCVars()
{

	plc[0] = (plc_high_frequency / 1000) & 0xFF;
	plc[1] = (plc_low_frequency / 1000) & 0xFF;
	plc[2] = plc_tx_gain;
	plc[3] = plc_tx_gain_f;

	plc[4] = (plc_timeout_uc / 1000) >> 8;
	plc[5] = plc_timeout_uc / 1000;

	plc[6] = (plc_timeout_net / 1000) >> 8;
	plc[7] = plc_timeout_net / 1000;
	
	for (int r = 0; r < 16; r++) // Loading SS_key
	{
		plc[r + 8] = plc_SS_key[r];
	}
	plc[24] = master_id;
	plc[25] = plc_modulation;
}

unsigned char readMEMuid(unsigned int address)
{
	unsigned char dataEEPROM;
	digitalWrite(UID_MEM_CS_PIN, LOW);
	SPI.transfer(READ);
	SPI.transfer((char)(address));
	dataEEPROM = SPI.transfer(0xFF);
	digitalWrite(UID_MEM_CS_PIN, HIGH);
	delay(10);
	//SPI.endTransaction();
	return dataEEPROM;
}

unsigned char *getMacUid()
{
	mac[0] = readMEMuid(0xFA);
	mac[1] = readMEMuid(0xFB);
	mac[2] = readMEMuid(0xFC);
	mac[3] = readMEMuid(0xFD);
	mac[4] = readMEMuid(0xFE);
	mac[5] = readMEMuid(0xFF);

	return mac;
}

void loadGSMParamFromVars()
{
	int index = 0; // server out ip

	for (int i = 0; i < SERVEROUT.length(); i++)
	{
		gsm_parameters[index++] = SERVEROUT.c_str()[i];
	}

	index = 40; //apn name

	gsm_parameters[index++] = SERVEROUT.length();

	for (int i = 0; i < GPRS_APN.length(); i++)
	{
		gsm_parameters[index++] = GPRS_APN.c_str()[i];
	}

	index = 80; // user name
	gsm_parameters[index++] = GPRS_APN.length();

	for (int i = 0; i < GPRS_LOGIN.length(); i++)
	{
		gsm_parameters[index++] = GPRS_LOGIN.c_str()[i];
	}

	index = 90; // apn pass

	gsm_parameters[index++] = GPRS_LOGIN.length();

	for (int i = 0; i < GPRS_PASSWORD.length(); i++)
	{
		gsm_parameters[index++] = GPRS_PASSWORD.c_str()[i];
	}

	index = 100;
	gsm_parameters[index++] = GPRS_PASSWORD.length();

	for (int i = 0; i < GPRS_PORTOUT.length(); i++)
	{
		gsm_parameters[index++] = GPRS_PORTOUT.c_str()[i];
	}

	//server param
	index = 110; //server ip

	gsm_parameters[index++] = GPRS_PORTOUT.length();

	for (int i = 0; i < SERVER_IP.length(); i++)
	{
		gsm_parameters[index++] = SERVER_IP.c_str()[i];
	}

	index = 150; //path out

	gsm_parameters[index++] = SERVER_IP.length();

	for (int i = 0; i < PATHOUT.length(); i++)
	{
		gsm_parameters[index++] = PATHOUT.c_str()[i];
	}

	index = 190;

	gsm_parameters[index++] = PATHOUT.length();

	for (int i = 0; i < PHONE1.length(); i++)
	{
		gsm_parameters[index++] = PHONE1.c_str()[i];
	}

	index = 205;
	gsm_parameters[index++] = PHONE1.length();

	for (int i = 0; i < PHONE2.length(); i++)
	{
		gsm_parameters[index++] = PHONE2.c_str()[i];
	}

	index = 220;
	gsm_parameters[index++] = PHONE2.length();

	for (int i = 0; i < PHONE3.length(); i++)
	{
		gsm_parameters[index++] = PHONE3.c_str()[i];
	}
	index = 235; //gprs port in
	gsm_parameters[index++] = PHONE3.length();

	//gsm_parameters[index++] = (GPRS_PORTIN >> 8 * 3) && 0xFF;
	//gsm_parameters[index++] = (GPRS_PORTIN >> 8 * 2) && 0xFF;
	//gsm_parameters[index++] = (GPRS_PORTIN >> 8) && 0xFF;
	//gsm_parameters[index++] = (GPRS_PORTIN) && 0xFF;
	//gsm_parameters[index++] = 4;
	// limit index 256
}

void loadGSMParamToVar()
{
	int index = 0;

	SERVEROUT = "";
	for (int i = 0; i < gsm_parameters[40]; i++)
	{
		SERVEROUT += (char)gsm_parameters[i];
	}

	//Serial.println("SERVEROUT: " + SERVEROUT );

	GPRS_APN = "";

	for (int i = 0; i < gsm_parameters[80]; i++)
	{
		GPRS_APN += (char)gsm_parameters[i + 41];
	}

	//Serial.println("GPRS_APN: " + GPRS_APN);

	GPRS_LOGIN = "";
	for (int i = 0; i < gsm_parameters[90]; i++)
	{
		GPRS_LOGIN += (char)gsm_parameters[i + 81];
	}

	//Serial.println("GPRS_LOGIN: " + GPRS_LOGIN);
	// apn pass

	GPRS_PASSWORD = "";

	for (int i = 0; i < gsm_parameters[100]; i++)
	{
		GPRS_PASSWORD += (char)gsm_parameters[i + 91];
	}

	//Serial.println("GPRS_PASSWORD: " + GPRS_PASSWORD);

	index = 101;

	GPRS_PORTOUT = "";
	for (int i = 0; i < gsm_parameters[110]; i++)
	{
		GPRS_PORTOUT += (char)gsm_parameters[i + 101];
	}

	//Serial.println("GPRS_PORTOUT: " + GPRS_PORTOUT);
	//server param
	//server ip

	index = 110;
	SERVER_IP = "";
	for (int i = 0; i < gsm_parameters[150]; i++)
	{
		SERVER_IP += (char)gsm_parameters[i + 111];
	}
	//Serial.println("SERVER_IP: " + SERVER_IP);

	PATHOUT = "";

	for (int i = 0; i < gsm_parameters[190]; i++)
	{
		PATHOUT += (char)gsm_parameters[i + 151];
	}

	//Serial.println("PATHOUT: " + PATHOUT);

	PHONE1 = "";
	for (int i = 0; i < gsm_parameters[205]; i++)
	{
		PHONE1 += (char)gsm_parameters[i + 191];
	}

	//Serial.println("PHONE1: " + PHONE1);
	PHONE2 = "";
	for (int i = 0; i < gsm_parameters[220]; i++)
	{
		PHONE2 += (char)gsm_parameters[i + 206];
	}
	//Serial.println("PHONE2: " + PHONE2);
	PHONE3 = "";
	for (int i = 0; i < gsm_parameters[235]; i++)
	{
		PHONE3 += (char)gsm_parameters[i + 221];
	}
	//Serial.println("PHONE3: " + PHONE3);
	index = 240;

	GPRS_PORTIN = (gsm_parameters[index++] << 8 * 3) && 0xFF;
	GPRS_PORTIN = (gsm_parameters[index++] << 8 * 2) && 0xFF;
	GPRS_PORTIN = (gsm_parameters[index++] << 8) && 0xFF;
	GPRS_PORTIN = gsm_parameters[index++] && 0xFF;

	//Serial.println("GPRS_PORTIN: " + String(GPRS_PORTIN));

	// limit index 256
}

void saveGSMParam(void)
{
	for (int r = 0; r < 256; r++)
	{
		writeMEM(GSM_PARAMETERS_INDEX + r, NORM, SPISettings_NORM, gsm_parameters[r]);
		//	Serial.print( String(gsm_parameters[r],HEX) + " ");
		//delay(10);
	}
	///Serial.println("");
}

void readGSMParamFromMem(void)
{
	for (int r = 0; r < 256; r++)
	{
		gsm_parameters[r] = readMEM(GSM_PARAMETERS_INDEX + r, NORM, SPISettings_NORM);
		//Serial.print(String(gsm_parameters[r],HEX) + " ");
		//delay(10);
	}
	//Serial.println("");
}

//template <class T> int EEPROM_writeAnything(int ee, const T& value)
//{
//const byte* p = (const byte*)(const void*)&value;
//unsigned int i;
//for (i = 0; i < sizeof(value); i++)
//
//writeMEM(ee++,  NORM, SPISettings_NORM, *p++);
//
//return i;
//}
//
//template <class T> int EEPROM_readAnything(int ee, T& value)
//{
//byte* p = (byte*)(void*)&value;
//unsigned int i;
//for (i = 0; i < sizeof(value); i++)
//*p++ =  readMEM(ee++, NORM, SPISettings_NORM);
//return i;
//}

void addToMEMWithUnix(
	unsigned int id,
	unsigned long s_number,
	unsigned long m_number,
	unsigned long p_number,
	unsigned long unixtime,
	unsigned char isFather)
{
	//unsigned int pos = memory_index +  (id * 0x0F) - 0x0C;

	meter_index = MEMORY_INDEX + readMEM(MEMORY_INDEX, NORM, SPISettings_NORM) * (0x0F /*+0x08*/) + 0x01;
	//1000 +( 1 * 23   ) + 1
	//1000 +( 2 * 23   ) + 1
	// Add Index of the meter in Memory
	Serial.print("meter_index: ");
	Serial.println(meter_index, DEC);
	writeMEM(meter_index + 1, NORM, SPISettings_NORM, id);

	// Add activity status "TRUE: 1"
	writeMEM(meter_index + 2, NORM, SPISettings_NORM, 0x01);

	// Add to Memory Board Serial Number

	char four = (s_number & 0xFF);
	char three = ((s_number >> 8) & 0xFF);
	char two = ((s_number >> 16) & 0xFF);
	char one = ((s_number >> 24) & 0xFF);

	writeMEM(meter_index + 3, NORM, SPISettings_NORM, one);
	writeMEM(meter_index + 4, NORM, SPISettings_NORM, two);
	writeMEM(meter_index + 5, NORM, SPISettings_NORM, three);
	writeMEM(meter_index + 6, NORM, SPISettings_NORM, four);

	// Add to Memory Meter Serial Number

	four = (m_number & 0xFF);
	three = ((m_number >> 8) & 0xFF);
	two = ((m_number >> 16) & 0xFF);
	one = ((m_number >> 24) & 0xFF);

	writeMEM(meter_index + 7, NORM, SPISettings_NORM, one);
	writeMEM(meter_index + 8, NORM, SPISettings_NORM, two);
	writeMEM(meter_index + 9, NORM, SPISettings_NORM, three);
	writeMEM(meter_index + 10, NORM, SPISettings_NORM, four);

	// Add to Memory Father Serial Number

	four = (p_number & 0xFF);
	three = ((p_number >> 8) & 0xFF);
	two = ((p_number >> 16) & 0xFF);
	one = ((p_number >> 24) & 0xFF);

	writeMEM(meter_index + 11, NORM, SPISettings_NORM, one);
	writeMEM(meter_index + 12, NORM, SPISettings_NORM, two);
	writeMEM(meter_index + 13, NORM, SPISettings_NORM, three);
	writeMEM(meter_index + 14, NORM, SPISettings_NORM, four);

	writeMEM(meter_index + 15, NORM, SPISettings_NORM, isFather);
	// Add to Memory unixtime

	four = (unixtime & 0xFF);
	three = ((unixtime >> 8) & 0xFF);
	two = ((unixtime >> 16) & 0xFF);
	one = ((unixtime >> 24) & 0xFF);

	writeMEM(meter_index + 16, NORM, SPISettings_NORM, one);
	writeMEM(meter_index + 17, NORM, SPISettings_NORM, two);
	writeMEM(meter_index + 18, NORM, SPISettings_NORM, three);
	writeMEM(meter_index + 19, NORM, SPISettings_NORM, four);

	// Add to Memory unixtime

	//four = (unixtime & 0xFF);
	//three = ((unixtime >> 8) & 0xFF);
	//two = ((unixtime >> 16) & 0xFF);
	//one = ((unixtime >> 24) & 0xFF);
	//
	//writeMEM(meter_index + 20, NORM, SPISettings_NORM, one);
	//writeMEM(meter_index + 21, NORM, SPISettings_NORM, two);
	//writeMEM(meter_index + 22, NORM, SPISettings_NORM, three);
	//writeMEM(meter_index + 23, NORM, SPISettings_NORM, four);

	//****** Add to Memory if the meter is a father (Not a father by default)* old**
	// adding father state

	//updating and storing meter quantity
	meters_qty += 1;
	writeMEM(MEMORY_INDEX, NORM, SPISettings_NORM, meters_qty);
}

void updateMeterInMEMWithUnix(
	unsigned int id,
	unsigned long s_number,
	unsigned long m_number,
	unsigned long p_number,
	unsigned long unixtime)
{
	unsigned int _meter_index = MEMORY_INDEX + (id * (0x0F /* + 0x08*/)) - (0x0C /*+0x08*/);
	//   1000 + (1 * 23) - 20
	//_meter_index = memory_index + readMEM(memory_index, NORM, SPISettings_NORM) * 0x0F + 0x01;

	// Add Index of the meter in Memory
	//Serial.print("_meter_index: ");
	//Serial.println(_meter_index, DEC);
	writeMEM(_meter_index + 1, NORM, SPISettings_NORM, id);

	// Add activity status "TRUE: 1"
	writeMEM(_meter_index + 2, NORM, SPISettings_NORM, 0x01);

	// Add to Memory Board Serial Number

	char four = (s_number & 0xFF);
	char three = ((s_number >> 8) & 0xFF);
	char two = ((s_number >> 16) & 0xFF);
	char one = ((s_number >> 24) & 0xFF);

	writeMEM(_meter_index + 3, NORM, SPISettings_NORM, one);
	writeMEM(_meter_index + 4, NORM, SPISettings_NORM, two);
	writeMEM(_meter_index + 5, NORM, SPISettings_NORM, three);
	writeMEM(_meter_index + 6, NORM, SPISettings_NORM, four);

	// Add to Memory Meter Serial Number

	four = (m_number & 0xFF);
	three = ((m_number >> 8) & 0xFF);
	two = ((m_number >> 16) & 0xFF);
	one = ((m_number >> 24) & 0xFF);

	writeMEM(_meter_index + 7, NORM, SPISettings_NORM, one);
	writeMEM(_meter_index + 8, NORM, SPISettings_NORM, two);
	writeMEM(_meter_index + 9, NORM, SPISettings_NORM, three);
	writeMEM(_meter_index + 10, NORM, SPISettings_NORM, four);

	// Add to Memory Father Serial Number

	four = (p_number & 0xFF);
	three = ((p_number >> 8) & 0xFF);
	two = ((p_number >> 16) & 0xFF);
	one = ((p_number >> 24) & 0xFF);

	writeMEM(_meter_index + 11, NORM, SPISettings_NORM, one);
	writeMEM(_meter_index + 12, NORM, SPISettings_NORM, two);
	writeMEM(_meter_index + 13, NORM, SPISettings_NORM, three);
	writeMEM(_meter_index + 14, NORM, SPISettings_NORM, four);

	// Add to Memory unixtime
	//
	//four = (unixtime & 0xFF);
	//three = ((unixtime >> 8) & 0xFF);
	//two = ((unixtime >> 16) & 0xFF);
	//one = ((unixtime >> 24) & 0xFF);
	//
	//writeMEM(meter_index + 20, NORM, SPISettings_NORM, one);
	//writeMEM(meter_index + 21, NORM, SPISettings_NORM, two);
	//writeMEM(meter_index + 22, NORM, SPISettings_NORM, three);
	//writeMEM(meter_index + 23, NORM, SPISettings_NORM, four);

	// Add to Memory if the meter is a father (Not a father by default)
	//writeMEM(_meter_index + 15, NORM, SPISettings_NORM, 0x00);

	//updating and storing meter quantity
	//meters_qty += 1;
	//writeMEM(memory_index, NORM, SPISettings_NORM, meters_qty);
}

void loadTaskParamFromVars()
{
	unsigned char i = 0;
	TimesForTask[i++] = minute_for_energy;
	TimesForTask[i++] = minute_for_demand;
	TimesForTask[i++] = minute_for_events;
	TimesForTask[i++] = minutes_for_allTask;
}

void saveTaskParam(void)
{
	// start index 800
	for (int r = 0; r < 5; r++)
	{
		writeMEM(TIME_FOR_TASK_INDEX + r, NORM, SPISettings_NORM, TimesForTask[r]);
	}
}

void readTaskParamFromMem(void)
{

	for (int r = 0; r < 5; r++)
	{
		TimesForTask[r] = readMEM(TIME_FOR_TASK_INDEX + r, NORM, SPISettings_NORM);
		//Serial.println(String(r) + "-" + String(TimesForTask[r]));
	}
}

void loadTaskParamToVar(void)
{

	minute_for_energy = TimesForTask[0];
	minute_for_demand = TimesForTask[1];
	minute_for_events = TimesForTask[2];
	minutes_for_allTask = TimesForTask[3];

	Serial.println("minute_for_energy: " + String(minute_for_energy));
	Serial.println("minute_for_demand: " + String(minute_for_demand));
	Serial.println("minute_for_events: " + String(minute_for_events));
	Serial.println("minutes_for_allTask: " + String(minutes_for_allTask));
}

unsigned char getMaintenanceTask(unsigned char minute)
{

	return master_maintenance_tasks[minute];
}

bool addMaintenanceTask(unsigned char minute, unsigned char cmd)
{

	master_maintenance_tasks[minute] = cmd;

	writeMEM(MAINTENANCE_TASK_INDEX + minute - 1, NORM, SPISettings_NORM, cmd);
}

void saveMaintenanceTaskToMEM(void)
{

	for (int r = 0; r < 60; r++)
	{
		writeMEM(MAINTENANCE_TASK_INDEX + r, NORM, SPISettings_NORM, master_maintenance_tasks[r]);
		//Serial.println(String(r) + "-" + String(master_maintenance_tasks[r]));
	}
}

void readMaintenanceTaskFromMEM(void)
{

	for (int r = 0; r < 60; r++)
	{
		master_maintenance_tasks[r] = readMEM(MAINTENANCE_TASK_INDEX + r, NORM, SPISettings_NORM);
		//Serial.println(String(r) + "-" + String(master_maintenance_tasks[r]));
	}
}

MaintenanceTask readMaintenanceTaskFromMEM(unsigned char minute)
{
	MaintenanceTask task;
	unsigned char array[6];
	unsigned char i = 0;
	unsigned int base = MAINTENANCE_TASK_INDEX + (minute * 6);
	for (size_t i = 0; i < sizeof(array); i++)
	{
		array[i] = readMEM(base + i, NORM, SPISettings_NORM);
		//Serial.print(array[i]);
		//Serial.print("-");
	}
	//Serial.print("");
	task.cmd_plc = array[i++];
	task.arg = array[i++];
	task.arg1 = array[i++];
	task.arg2 = array[i++];
	task.arg3 = array[i++];
	task.arg4 = array[i++];
	return task;
}

void writeMaintenanceTaskToMEM(unsigned char minute,
							   unsigned char cmd_plc,
							   unsigned char arg,
							   unsigned char arg1,
							   unsigned char arg2,
							   unsigned char arg3,
							   unsigned char arg4)
{

	unsigned char array[6];
	unsigned char i = 0;
	array[i++] = cmd_plc;
	array[i++] = arg;
	array[i++] = arg1;
	array[i++] = arg2;
	array[i++] = arg3;
	array[i++] = arg4;

	unsigned int base = MAINTENANCE_TASK_INDEX + (minute * 6);

	for (size_t i = 0; i < sizeof(array); i++)
	{
		writeMEM(base + i, NORM, SPISettings_NORM, array[i]);
		Serial.print(array[i]);
		Serial.print("-");
	}
	Serial.print("");
}

void clearAllMaintenanceTask()
{
	for (size_t i = 0; i < 60 * 6; i++)
	{
		writeMEM(MAINTENANCE_TASK_INDEX + i, NORM, SPISettings_NORM, 0xFF);
	}
}

void showMaintenanceTaskToConsole()
{

	Serial.println("min cmd arg0 arg1 arg2 arg3 arg4");

	for (unsigned char i = 0; i < 60; i++)
	{
		MaintenanceTask task = readMaintenanceTaskFromMEM(i);
		Serial.print(i);
		Serial.print(" - ");
		Serial.print(task.arg);
		Serial.print(" - ");
		Serial.print(task.arg1);
		Serial.print(" - ");
		Serial.print(task.arg2);
		Serial.print(" - ");
		Serial.print(task.arg3);
		Serial.print(" - ");
		Serial.println(task.arg4);
	}
}

void saveChageFatherParamToMem()
{

	int address = 21000;
	writeMEM(address++, NORM, SPISettings_NORM, (master_change_father_time >> 8 & 0xFF));
	writeMEM(address++, NORM, SPISettings_NORM, (master_change_father_time & 0xFF));
	writeMEM(address++, NORM, SPISettings_NORM, (meter_change_father_time >> 8 & 0xFF));
	writeMEM(address++, NORM, SPISettings_NORM, (meter_change_father_time & 0xFF));
	writeMEM(address++, NORM, SPISettings_NORM, (incomunication_task_time >> 8 & 0xFF));
	writeMEM(address++, NORM, SPISettings_NORM, (incomunication_task_time & 0xFF));
}

void loadChageFatherParamToMem()
{

	int address = 21000;

	master_change_father_time = (readMEM(address++, NORM, SPISettings_NORM) << 8) | readMEM(address++, NORM, SPISettings_NORM);
	meter_change_father_time = (readMEM(address++, NORM, SPISettings_NORM) << 8) | readMEM(address++, NORM, SPISettings_NORM);
	incomunication_task_time = (readMEM(address++, NORM, SPISettings_NORM) << 8) | readMEM(address++, NORM, SPISettings_NORM);

	Serial.println("master_change_father_time: " + String(master_change_father_time));
	Serial.println("meter_change_father_time: " + String(meter_change_father_time));
	Serial.println("incomunication_task_time: " + String(incomunication_task_time));
}