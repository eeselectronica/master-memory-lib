/*
* MEM.h
*
* Created: 11/16/2017 9:17:27 AM
*  Author: Ing.Elvys Collado
*/


#ifndef MEM_H_
#define MEM_H_

#include <Arduino.h>
#include <Ethernet.h>
#include <utility/w5100.h>
#include "common_header.h"
#include <SPI.h>
#include "linked_list.h"

//char plc[50];//////////////////////////////////////////////// Array where plc parameters are stored at 8bit resolution
//char task[50];/////////////////////////////////////////////// Array where tasks parameters are stored at 8bit resolution

#define  MEMORY_INDEX  1000

#define RECORD_SIZE		19
 struct MaintenanceTask
 {
	 unsigned char minute;
	 unsigned char cmd_plc;
	 unsigned char arg;
	 unsigned char arg1;
	 unsigned char arg2;
	 unsigned char arg3;
	 unsigned char arg4;
 };

	
struct MEMORY
{
	unsigned char slave_pin;														// Physical Chip Select Pin
	unsigned char size;																// Amount of address bytes

};

/*		addToMEM			:write meters serial numbers to memory												 */
void						addToMEM(
unsigned int id,							// Id to assign in memory
unsigned long s_number,						// Meter Board Serial Number
unsigned long m_number,						// Meter Serial Number
unsigned long p_number,						// Meter Father Serial Number (repetition mode)
unsigned long p_number2,					// Meter Father Serial Number (repetition mode) 2
unsigned char isFather);					

/*		addToMEM			:write meters serial numbers to memory												 */
void						updateMeterInMEM(
unsigned int id,							// Id to assign in memory
unsigned long s_number,						// Meter Board Serial Number
unsigned long m_number,						// Meter Serial Number
unsigned long p_number,						// Meter Father Serial Number (repetition mode)
unsigned long p_number2);					

/*		changeMetersState	:write on selected memory at requested address										 */
void						changeMetersState(
unsigned int id,
unsigned char state);
/*		copyMEMToRAM	    :copy all active meter in MEM to RAM												 */
linked_list					copyMEMToRAM(
linked_list _list);

/*		disposeMEM			:erase all meters from memory														 */
void						disposeMEM();

/*		getMEMIndex			:return the amount of meters in MEM													 */
unsigned char               getMEMIndex();

/*		loadNetworkParam	:load Network Parameters from MEM to RAM											 */
void						loadNetworkParam(void);

/*		loadNetworkParam	:load Network Parameters from MEM to RAM											 */
void						loadPLCParam(void);

/*		MEMConfig		    :Configuration for SPI Memory														 */
struct MEMORY				MEMconfig(unsigned char size, unsigned char slave_pin);

/* printSerialListFromMEM	:print through serial the list of meters in MEM										 */
void						printSerialListFromMEM( );

/*		readMEM				:returns requested data from address on selected MEM								 */
unsigned char				readMEM(
unsigned long address,						// Address of byte requested
MEMORY mem,									// struct containing Physical Pin for Chip Select and Address Size in bytes
SPISettings _SPISettings);					// Standard SPI configuration

/*		saveNetworkParam	:save Network Parameters at MEM														 */
void						saveNetworkParam(void);

/*		savePLCParam		:save PLC Parameters at MEM														 */
void						savePLCParam(void);

/*	setInitialConditions	:Save to MEM father byte															 */
unsigned char				setFatherEEPROM(
unsigned long p_number);
/*	setInitialConditions	:Save to MEM all initial conditions													 */
void						setInitialConditions( void );

/*		updateFather		:update the father of one meter on MEM												 */
unsigned char				updateFatherEEPROM(
unsigned long s_number,
unsigned long p_number,
unsigned char father_selector);


void updateFatherMEM(unsigned int id, unsigned char selector);

/*		writeMEM			:write on selected memory at requested address										 */
void						writeMEM(
unsigned int address,						// Address of byte requested
MEMORY mem,									// struct containing Physical Pin for Chip Select and Address Size in bytes
SPISettings _SPISettings,					// Standard SPI configuration
unsigned char data);						// Data to be write on memory

/*		getNetInJSON		:return network array in
format 												 */
String						getNetInJSON( void );

/*		getPLCInJSON		:return plc array in json format 													 */
String						getPLCInJSON( void );

/*		readParamFromMEM	:load all from MEM																	 */
void						readParamFromMEM(void);

void						loadGSMParamFromVars();

void						saveGSMParam(void);

void						readGSMParamFromMem(void);

void						loadGSMParamToVar(void);
													 
void						loadTaskParamFromVars();

void						saveTaskParam(void);

void						readTaskParamFromMem(void);

void						loadTaskParamToVar(void);

unsigned char  * 			getMacUid(void);

   	void addToMEMWithUnix(
	unsigned int id,
	unsigned long s_number,
	unsigned long m_number,
	unsigned long p_number,
	unsigned long unixtime,
	unsigned char isFather);
									
void updateMeterInMEMWithUnix(
	unsigned int id,
	unsigned long s_number,
	unsigned long m_number,
	unsigned long p_number,
	unsigned long unixtime
);
		
void saveMaintenanceTaskToMEM(void);
void readMaintenanceTaskFromMEM(void);
unsigned char getMaintenanceTask(unsigned char minute);
bool addMaintenanceTask(unsigned char minute,  unsigned char cmd);
void clearAllMaintenanceTask();
void writeMaintenanceTaskToMEM(unsigned char minute,
								   unsigned char cmd_plc,
								   unsigned char arg,
								   unsigned char arg1,
								   unsigned char arg2,
								   unsigned char arg3,
								   unsigned char arg4);		
								   					
MaintenanceTask readMaintenanceTaskFromMEM(unsigned char minute);
void showMaintenanceTaskToConsole();
void	saveChageFatherParamToMem();
void 			loadChageFatherParamToMem();

#endif /* MEM_H_ */